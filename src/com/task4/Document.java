package com.task4;

import java.util.*;

public class Document {
    public int id;
    public String name;
    public int pageCount;

    public Document(int id, String name, int pageCount) {
        this.id = id;
        this.name = name;
        this.pageCount = pageCount;
    }

    public static void main(String[] args) {
        List<Document> list = new ArrayList<>();
        list.add(new Document(1, "Report-1", 55));
        list.add(new Document(2, "Report-2", 110));
        list.add(new Document(3, "Report-3", 30));
        list.add(new Document(4, "Report-4", 12));

        System.out.println(organizeDocuments(list));

    }

    public static Map<Integer, Document> organizeDocuments(List<Document> documents) {
        HashMap<Integer, Document> result = new HashMap<>();
        for(Document el : documents){
            result.put(el.id, el);
        }
        return result;
    }

    @Override
    public String toString() {
        return "id{" +
                ", name='" + name + '\'' +
                ", pageCount=" + pageCount +
                '}';
    }
}

