package com.task3;


import java.util.HashSet;
import java.util.Set;

public class PowerfulSet {
    public static void main(String[] args) {
        Set<Integer> set1 = new HashSet<>();
        set1.add(1);
        set1.add(2);
        set1.add(3);
        Set<Integer> set2 = new HashSet<>();
        set2.add(0);
        set2.add(1);
        set2.add(2);
        set2.add(4);

        System.out.println(intersection(set1, set2));
        set1.add(3);
        System.out.println(union(set1, set2));
        System.out.println(relativeComplement(set1,set2));

    }

    public static <T> Set<T> intersection(Set<T> set1, Set<T> set2) {
        Set<T> set3 = new HashSet<>();
        set1.retainAll(set2);
        for (T el : set1) {
            set3.add(el);
        }
        return set3;
    }

    public static <T> Set<T> union(Set<T> set1, Set<T> set2) {
        Set<T> set3 = new HashSet<>();
        set3.addAll(set1);
        set3.addAll(set2);
        return set3;
    }
    public static <T> Set<T> relativeComplement(Set<T> set1, Set<T> set2){
        Set<T> set3 = new HashSet<>();
        set1.removeAll(set2);
        for (T el : set1) {
            set3.add(el);
        }
        return set3;
    }
}
