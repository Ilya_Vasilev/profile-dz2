package com.task1;

import java.util.ArrayList;
import java.util.HashSet;

public class SetOfUniqueElements {
    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(3);
        list.add(4);
        list.add(4);
        list.add(4);
        list.add(5);
        list.add(5);
        list.add(6);
        System.out.println(setOfUniqueElements(list));

    }

    public static <T> HashSet<T> setOfUniqueElements(ArrayList<T> from) {
        HashSet<T> result = new HashSet<>();
        result.addAll(from);
        return result;
    }


}
